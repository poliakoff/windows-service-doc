# Getting Started

### Iriscan service set up on Windows (tested on 64-bit system)

1. If there is a JDK/JRE in the system prior to version 11, remove it. This can be checked with the `cmd` command `java -version`

2. Install JDK v 11+ from the official website or using the following link: [64 bit version JDK v11](https://drive.google.com/file/d/1hfRpsCmnydCMTia1Bc4hOSJxaSNX6EA7/view?usp=sharing).

3. Update `JAVA_HOME` system variable to point at the JDK directory. Run `cmd` command as admin `setx JAVA_HOME -m "C:\Program Files\Java\jdk-11.0.7"` (if you have JDK installed at the different location, update JDK path accordingly).

4. Update `PATH` system variable to contain path to JDK binaries: 
    - Open Start menu and search for Advanced System Settings
    - Launch View advanced system settings
    - Go to Environment Variables
    - Press “Edit…” for Path system variable and add new path `%JAVA_HOME%\bin` or change already existing path to Java.

5. Verify with cmd `java -version`. The following output is needed:
    ```
    C:\Users\Phoenix>java -version
    java version "11.0.7" 2020-04-14 LTS
    Java(TM) SE Runtime Environment 18.9 (build 11.0.7+8-LTS)
    Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.7+8-LTS, mixed mode)
    ```
    Where java version is 11+.

6. If there is IriService installed the system, stop it in the system services.

7. Download the Iriscan service executables using the following link: [Iriscan service executables](https://drive.google.com/drive/folders/1beaZosUyUNYKW_hh7GyjBKEF0Kov6xC8?usp=sharing) and place them together in one folder.

8. Connect the Iritech device to the system. Ensure Iritech device drivers are present in the system. If not, install. Drivers can be downloaded from the Iritech website or using the following link: [Iritech drivers](https://drive.google.com/file/d/1R28hNhR-Zew0keQCBY-ktRZUn-GzLZgy/view?usp=sharing).

9. Execute cmd command `java -jar service-0.0.1-SNAPSHOT.jar` from the above mentioned folder with the Iriscan service executable files.
 Now, if there are log entries in the console, which do not indicate error, the Iriscan server for the device communication should be up and running. The Iriscan web application should be able to communicate with it as required.
 



